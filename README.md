# MyHome Arduino Projects

This is a personal collection of arduino projects for various applications using either NodeMCU or ESP32-CAM boards.

These projects serve as clients for the overall home automation 'MyHome' project. Each project serves to automate a different system, for now an aquarium and a garden.
